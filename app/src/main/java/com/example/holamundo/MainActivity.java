package com.example.holamundo;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText etNombre;
    private TextView tvMensaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Recojo los elementos del layout
        etNombre = (EditText) findViewById(R.id.etNombre);
        tvMensaje = (TextView) findViewById(R.id.tvMensaje);
    }

    //Metodo para el boton pulsar
    public void pulsar(View v){
        //trim() quita los espacios del principio y final
        String mens = etNombre.getText().toString().trim();
        if(mens.length() == 0){
            Toast.makeText(this,"Debes poner un nombre", Toast.LENGTH_LONG).show();
            return;
        }
        tvMensaje.setText(mens);
        etNombre.setText("");
    }
}